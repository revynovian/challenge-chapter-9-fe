import "./Home.css";
import React from "react";
import Arrow from "../../assets/arrow.png";
import Thegames from "./the-games/Thegames";
import Requirements from "./requirements/Requirements";
import Features from "./features/Features";
import Scores from "./scores/Scores";
import Subscribe from "./subscribe/Subscribe";
import { Container, Row, Col, Image, Button } from "react-bootstrap";
import { Link } from "react-router-dom";

const Home = () => {
  return (
    <div>
      <div className="jumbotron jumbotron-fluid text-center main-page header--custom">
        <Container className="custom-container">
          <Row style={{ marginTop: "200px" }}>
            <Col>
              <h1 className="display-4 header--animation_a">PLAY TRADITIONAL GAME</h1>
              <h2 className="lead header--animation_b">Experience new traditional game play</h2>
              <Button className="mt-5 custom-home-button" style={{color: "black", padding:"15px", borderRadius:"25px", backgroundColor:"#F3AF34", border:"none"}} as={Link} to="/games/rps/play" role="button">
                <strong>PLAY NOW</strong>
              </Button>
            </Col>
          </Row>
          <Row style={{ marginTop: "300px" }}>
            <Col>
              <p className="text">THE STORY</p>
              <a href="#games" className="rounded">
                <Image src={Arrow} style={{ width: "20px" }} alt="" />
              </a>
            </Col>
          </Row>
        </Container>
      </div>
      <Thegames />
      <Requirements />
      <Features />
      <Scores />
      <Subscribe />
    </div>
  );
};

export default Home;
