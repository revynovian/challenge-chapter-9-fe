import "./Thegames.css";
import React from "react";
import slide from "../../../assets/slider-1.jpg";
import slidetwo from "../../../assets/slider-2.jpg";
import slidethree from "../../../assets/slider-3.jpg";
import { Carousel, Container, Row, Col, Image } from "react-bootstrap";
import { useState } from "react";

const Thegames = () => {
  const [index, setIndex] = useState(0);

  const handleSelect = (selectedIndex, e) => {
    setIndex(selectedIndex);
  };
  return (
    <div className="jumbotron jumbotron-fluid games-page" id="games">
      <Container>
        <Row>
          <Col lg={4}>
            <p className="lead">What's so special?</p>
            <h1 className="display-4">THE GAMES</h1>
          </Col>
        </Row>

        <Row>
          <Col lg={8} className="slider-layout">
            <Carousel activeIndex={index} onSelect={handleSelect} slide={false} fade={false}>
              <Carousel.Item>
                <Image className="d-block w-100" src={slide} alt="First slide" />
              </Carousel.Item>
              <Carousel.Item>
                <Image className="d-block w-100" src={slidetwo} alt="Second slide" />
              </Carousel.Item>
              <Carousel.Item>
                <Image className="d-block w-100" src={slidethree} alt="Third slide" />
              </Carousel.Item>
            </Carousel>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default Thegames;
