module.exports = [
  {
    title: "Rock Paper Scissor",
    description : "Test your strategy against computer!",
    thumbnail: "https://i.ibb.co/jr3BxkZ/rps-thumbnail.png",
    price : "free",
    available: "ready",
    link: "/games/rps",
    coverImage: "https://i.ibb.co/6Hs1MP3/rps-cover.jpg"
  },
  {
    title: "Classic Snake",
    description : "Classic arcade snake game. Slither to the top of the leaderboard!",
    thumbnail : "https://i.ibb.co/rZ4XHpw/snake-thumbnail.png",
    price : "free", 
    available: "ready" ,
    link: "/games/snake" ,
    coverImage: "https://i.ibb.co/9tLt32Q/snake-cover.jpg"   
  },
  {
    title: "Tic Tac Toe",
    description : "Play the classic Tic-Tac-Toe game for free online",
    thumbnail : "https://i.ibb.co/34wztrG/tictactoe-thumbnail.png",
    price : "free",
    available: "ready",
    link: "/games/tictactoe" ,
    coverImage: "https://i.ibb.co/8sgTjk2/tictactoe-cover.jpg"
  }
];