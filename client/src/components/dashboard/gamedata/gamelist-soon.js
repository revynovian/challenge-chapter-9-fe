module.exports = [
    {
      title: "Sudoku",
      description : "Sudoku is a brain challenging number game, played on a 9x9 sudoku board",
      thumbnail: "https://i.ibb.co/5nyK3L6/sudoku-thumbnail.png",
      price : "free",
      available: "soon",
      link: ""
    },
    {
      title: "Chess",
      description : "Play chess online with million players from around the world! Enjoy free unlimited chess games",
      thumbnail : "https://i.ibb.co/tX7hKpP/chess-thumbnail.png",
      price : "premium", 
      available: "soon" ,
      link: ""    
    },
    {
      title: "2048",
      description : "2048 is a single-player sliding block puzzle game, even if you don't love numbers you will love this game",
      thumbnail : "https://i.ibb.co/Lhj1MRG/2048-thumbnail.png",
      price : "free",
      available: "soon",
      link: "" 
    },
    {
      title: "Chrome Dino",
      description : "Google's hidden T-Rex dinosaur game, guides a pixelated T-rex across a side-scrolling landscape",
      thumbnail : "https://i.ibb.co/KbvPqHn/chromedino-thumbnail.png",
      price : "free",
      available: "soon" ,
      link: ""
    }
  ];
