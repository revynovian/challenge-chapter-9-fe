import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import Axios from "axios";

import "./dashboard.css";
import { Container, Row, Col, Button} from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCalendarAlt, faEdit, faMapMarkerAlt } from "@fortawesome/free-solid-svg-icons";
import GameCards from "../dashboard/gamecard"

import GameList from "./gamedata/gamelist-library";
import GameListMore from "./gamedata/gamelist-soon";
import profile from "../../assets/sample-ava.jpg";

const Dashboard = () => {
  const userID = localStorage.getItem("userID");
  const accessToken = localStorage.getItem("token");
  const url = `https://backend-app-chapter-9.herokuapp.com/user/${userID}`;

  const [player, setPlayer] = useState({});
  const [playerDetail, setPlayerDetail] = useState({});
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    Axios.get(url, {
      headers: { Authorization: accessToken },
    })
      .then((res) => {
        // console.log(res.data.message)
        setIsLoading(true);
        const basicInfo = res.data.message;
        const detailInfo = res.data.message.User_Detail;
        setPlayer(basicInfo);
        setPlayerDetail(detailInfo);
      })
      .catch((error) => {
        console.log(error);
      });
  }, [url, accessToken]);

  // convert date to get player's joined date
  const getDate = new Date(player.createdAt);
  const joinDate = getDate.toLocaleString("default", { month: "long", year: "numeric" });

  // gameList Library-- statics data
    const gameListLibrary = GameList.map((e, i) => 
    <GameCards
    key={i}
    gameName={e.title} 
    gameDescription={e.description} 
    gameThumbnail={e.thumbnail} 
    gamePrice={e.price} 
    gameReady={e.available} 
    gameLink={e.link}
    />
  )

  // gameList coming soon-- statics data -- cant add new game yet 
  const gameListSoon = GameListMore.map((e, i) => 
    <GameCards
    key={i}
    gameName={e.title} 
    gameDescription={e.description} 
    gameThumbnail={e.thumbnail} 
    gamePrice={e.price} 
    gameReady={e.available} 
    gameLink={e.link}
    />
  )

  return (
    <div className="jumbotron jumbotron-fluid dashboard-custom py-3">
      <Container>
        <Row className="justify-content-center mb-3 text-white p-5 mt-5 profile-card" style={{ backgroundColor: "#383838" }}>
          <Col md={2}>
            <img className="profile" src={profile} alt="Avatar" />
          </Col>
          <Col md={8} className="placeholder-glow">
            <h2 className="text-warning">{isLoading ? player.username : <span className="text-muted rounded placeholder col-4" />}</h2>
            <h5 className="lead">{isLoading ? playerDetail.bio : <span className="text-muted rounded placeholder col-4" />}</h5>
            <h6 className="text-muted">
              <FontAwesomeIcon className="me-3" icon={faMapMarkerAlt} />
              {isLoading ? playerDetail.city : <span className="rounded placeholder col-3" />}
            </h6>
            <h6 className="text-muted">
              <FontAwesomeIcon className="me-3" icon={faCalendarAlt} />
              {isLoading ? `Joined ${joinDate}` : <span className="rounded placeholder col-3" />}
            </h6>
          </Col>
          <Col>
            <Link to="/profile" className="text-warning">
              <Button variant="warning" type="submit">
                <FontAwesomeIcon icon={faEdit} /> Edit Profile
              </Button>
            </Link>
          </Col>
        </Row>

      {/* Game List section -- static data -- can't add new game yet */}
        <Row className="my-5">
          <h2 className="text-white">Your Games</h2>
          {/* game library */}
          {gameListLibrary}
        </Row>
        <Row className="my-5">
          {/* game coming soon */}
          <h4 className="text-white">More Games Coming Soon!</h4>
            {gameListSoon}
        </Row>
      </Container>
    </div>
  );
};

export default Dashboard;
