import "./dashboard.css"
import React from "react";
import {Col, Card, Badge, Button} from "react-bootstrap";
import { Link } from "react-router-dom";

const Cards = (props) => {

  return (
    <>
      <Col md={3}>
            <Card style={{ width: "18rem", color: "white", backgroundColor: "#383838" }} className="das-card-custom">
              <Card.Img variant="top" src={props.gameThumbnail} />
              <Card.Body>
                <Card.Title>{props.gameName}</Card.Title>
                <Card.Text>{props.gameDescription}</Card.Text>
                <Badge pill style={{backgroundColor: "#1A1C1D" }}>{props.gamePrice}</Badge>
                <Col className="mt-3">
                  {(props.gameReady === "ready")? 
                  <Link to={props.gameLink} className="text-warning">
                    <Button variant="warning">Details</Button>
                  </Link> : 
                    <Badge pill style={{backgroundColor: "#5D5E5E" }}>{props.gameReady}</Badge>}
                </Col>
              </Card.Body>
            </Card>
          </Col>
    </>
  )
}

export default Cards;