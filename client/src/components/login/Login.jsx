import "./Login.css";
import React, { useState, useContext } from "react";
import Axios from "axios";
import { Container, Row, Col, Form, Button, Alert } from "react-bootstrap";
import { Link, useHistory } from "react-router-dom";

// Import context
import { myAuthContext } from "../../context/authContext";

const Login = () => {
  let history = useHistory();
  const { login } = useContext(myAuthContext);

  const url = "https://backend-app-chapter-9.herokuapp.com/user/login";
  const [usernameOrEmail, setUsernameOrEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState("");
  const [isLoading, setisLoading] = useState(false);

  const handlerLogin = (e) => {
    e.preventDefault();
    setisLoading(true);
    Axios.post(url, {
      usernameOrEmail: usernameOrEmail,
      password: password,
    })
      .then(async (res) => {
        // if (!res.status === 404) {
        // console.log("Success:", res);
        setisLoading(false);
        await login(res.data.accessToken, res.data.id);
        history.push("/dashboard");
        // } else {
        //   setError("Something went wrong. Please check your email or password");
        // }
      })
      .catch((error) => {
        if (error.response.status === 401 || error.response.status === 400) {
          setError(error.response.data);
        } else {
          setError("Something went wrong. Please try again later");
        }
        setisLoading(false);
        // console.error("Failed:", error);
      });
  };

  return (
    <div>
      <Container fluid className="login-page">
        <Row className="justify-content-center vh-100 align-items-center pb-5">
          <Col md={4} className="text-white p-5" style={{ backgroundColor: "#383838", borderRadius: "15px" }}>
            <h2 className="text-center">Login</h2>
            {error && (
              <Alert className="my-3" variant={"danger"}>
                {error}
              </Alert>
            )}

            <Form onSubmit={(e) => handlerLogin(e)}>
              <Form.Group className="mb-3">
                <Form.Label htmlFor="usernameOrEmail">Username or Email Address</Form.Label>
                <Form.Control type="text" id="usernameOrEmail" name="usernameOrEmail" value={usernameOrEmail} onChange={(e) => setUsernameOrEmail(e.target.value)} placeholder="Enter username or email" />
              </Form.Group>

              <Form.Group className="mb-5">
                <Form.Label htmlFor="password">Password</Form.Label>
                <Form.Control type="password" id="password" name="password" value={password} onChange={(e) => setPassword(e.target.value)} placeholder="Enter password" />
              </Form.Group>

              <Row className="mb-4">
                <Col className="text-center">
                  <Button variant="warning" type="submit" className="ps-5 pe-5" disabled={isLoading}>
                    <strong>{isLoading ? "Loading…" : "Login"}</strong>
                  </Button>
                </Col>
              </Row>

              <Row>
                <Col className="text-center text-link">
                  <p>Don't have an account?</p>
                  <Link to="/register" className="text-warning">
                    Create one for FREE!
                  </Link>
                </Col>
              </Row>
            </Form>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default Login;
