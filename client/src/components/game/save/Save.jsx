import "./Save.css";
import React, { useState, useEffect } from "react";
import { Container, Row, Col, Form, Button, Alert } from "react-bootstrap";

import Axios from "axios";
import { Link } from "react-router-dom";

const Save = ({ exampleScore }) => {
  // props score from Game
  const curentScore = exampleScore;

  // Alert
  const [success, setSuccess] = useState(false);

  // error massage
  // const [error, setError] = useState("");

  // fetch user data
  const [player, setPlayer] = useState({});
  const [score, setScore] = useState({});
  const userID = localStorage.getItem("userID");
  const accessToken = localStorage.getItem("token");
  const url = `https://backend-app-chapter-9.herokuapp.com/user/${userID}`;
  const urlUpdate = `https://backend-app-chapter-9.herokuapp.com/user/score/${userID}`;

  // get lastest score user from db
  useEffect(() => {
    Axios.get(url, {
      headers: { Authorization: accessToken },
    })
      .then((res) => {
        // setIsLoading(true);
        const basicInfo = res.data.message;
        const scorePlayer = res.data.message.User_Score;
        setPlayer(basicInfo);
        setScore(scorePlayer);
      })
      .catch((error) => {
        console.log(error);
      });
    // setIsLoading(false);
  }, [url, accessToken]);

  // update curent score
  const handleUpdateScore = (e) => {
    e.preventDefault();

    const body = {
      rock_paper_scissor: curentScore,
    };

    // console.log(body);

    Axios.put(urlUpdate, body, {
      headers: { Authorization: accessToken },
    })
      .then((res) => {
        // console.log(res.data.message);
        setSuccess(true);
      })
      .catch((error) => {
        if (error.response.status === 401 || error.response.status === 400) {
          // setError(error.response.data);
        } else {
          // setError("Something went wrong. Please try again later");
        }
      });
  };

  return (
    <div>
      <Container fluid className="save-page">
        <Row className="justify-content-center vh-100 align-items-center pb-5">
          {/* form update score */}
          <Col md={4} className="text-white p-5" style={{ backgroundColor: "#383838", borderRadius: "15px" }}>
            <h2 className="text-center">Score {player.username}</h2>
            {success && (
              <Alert className="my-3" variant={"success"}>
                Your score was successfully updated
              </Alert>
            )}

            {/* lastest score */}
            <Form className="text-center mt-4" onSubmit={(e) => handleUpdateScore(e)}>
              <Row className="mb-3">
                <Col sm={8}>
                  <Form.Label htmlFor="Score" className="col-form-label">
                    Your Last Score :
                  </Form.Label>
                </Col>
                <Col sm={4} style={{ position: "relative", right: "15%" }}>
                  <Form.Control type="text" id="lastScore" name="lastScore" value={score.rock_paper_scissor || "0"} readOnly />
                </Col>
              </Row>

              {/* curent score */}
              <Row className="mb-3">
                <Col sm={8}>
                  <Form.Label htmlFor="Score" className="col-form-label">
                    Your Curent Score :
                  </Form.Label>
                </Col>
                <Col sm={4} style={{ position: "relative", right: "15%" }}>
                  <Form.Control type="text" id="curentScore" name="curentScore" value={curentScore} onChange={(e) => setScore({ ...score, rock_paper_scissor: e.target.value })} readOnly />
                </Col>
              </Row>

              {/* button update and send to database */}
              <Row className="mb-4">
                <Col>
                  <Button variant="warning" type="submit" className="ps-5 pe-5">
                    <strong>Save</strong>
                  </Button>
                </Col>

                <Col>
                  <Button as={Link} to="/games/rps/play" variant="secondary" className="ps-4 pe-4">
                    <strong>Play Again?</strong>
                  </Button>
                </Col>
              </Row>
            </Form>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default Save;
