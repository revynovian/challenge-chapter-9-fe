import "./Game.css";
import React, { useState, useEffect } from "react";
import rock from "../../assets/batu.png";
import paper from "../../assets/kertas.png";
import scissors from "../../assets/gunting.png";
import refresh from "../../assets/refresh.png";
import logo from "../../assets/logo.png";
import info from "../../assets/info.png";
import coin from "../../assets/goldcoin.png";
import rules from "../../assets/rules.png";
import { Container, Row, Col, Image, Modal, Button } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes } from "@fortawesome/free-solid-svg-icons";

import Axios from "axios";
import { Link } from "react-router-dom";

const Game = (props) => {
  // rps game logic
  const [playerHands, setPlayerHands] = useState(null);
  const [cpuHands, setCpuHands] = useState(null);
  const [playerScore, setPlayerScore] = useState(0);
  const [cpuScore, setCpuScore] = useState(0);
  const [result, setResult] = useState(null);
  // contoh agregat skor
  // fetch from database
  const [exampleScore, setScore] = useState(null);

  const CHOICES = [
    {
      choice: "rock",
      beats: "scissor",
      losesTo: "paper",
    },
    {
      choice: "paper",
      beats: "rock",
      losesTo: "scissor",
    },
    {
      choice: "scissor",
      beats: "paper",
      losesTo: "rock",
    },
  ];

  const cpuPick = () => {
    return CHOICES[Math.floor(Math.random() * CHOICES.length)].choice;
  };

  const handleClick = (playerPick, cpuPick) => {
    setPlayerHands(playerPick);
    setCpuHands(cpuPick);
    const getResult = compareResult(playerPick, cpuPick);
    setResult(getResult);
    // console.log(getResult);
  };
  const compareResult = (player, cpu) => {
    let item = 0;
    for (item in CHOICES) {
      if (cpu === CHOICES[item].choice) {
        if (CHOICES[item].losesTo.includes(player)) {
          setPlayerScore(playerScore + 1);
          // calculate score(simple scoring => win + 10 pts)
          setScore(exampleScore + 10);
          return "YOU WIN";
        } else if (CHOICES[item].beats.includes(player)) {
          setCpuScore(cpuScore + 1);
          // calculate score(simple scoring => lose - 5 pts)
          setScore(exampleScore - 5);
          return "YOU LOSE";
        } else return "DRAW";
      }
    }
  };

  const refreshClick = () => {
    // reset score
    setPlayerScore(0);
    setCpuScore(0);
    setPlayerHands(null);
    setCpuHands(null);
    setResult(null);
  };

  // fetch user data
  const [player, setPlayer] = useState({});
  const [isLoading, setIsLoading] = useState(false);
  const userID = localStorage.getItem("userID");
  const accessToken = localStorage.getItem("token");
  const url = `https://backend-app-chapter-9.herokuapp.com/user/${userID}`;
  useEffect(() => {
    Axios.get(url, {
      headers: { Authorization: accessToken },
    })
      .then((res) => {
        // console.log(res.data.message)
        setIsLoading(true);
        const basicInfo = res.data.message;
        const scorePlayer = res.data.message.User_Score.rock_paper_scissor;
        // const detailInfo = res.data.message.User_Detail
        setPlayer(basicInfo);
        setScore(scorePlayer);
        // setPlayerDetail(detailInfo)
        // console.log(res);
      })
      .catch((error) => {
        console.log(error);
      });
    setIsLoading(false);
  }, [url, accessToken]);

  // bootstrap modal
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  return (
    <div className="game-page">
      <Container>
        {/* header-section */}
        <Row className="justify-content-center game-header_custom">
          <Col sm={6} className="d-flex">
            <Link to="/games/rps">
              <Image src={logo} style={{ width: "50px", height: "50px" }} className="logo-button" />
            </Link>
            <h2 className="ms-4 mt-2 fw-bold" style={{ color: "#F9B23D" }}>
              ROCK PAPER SCISSORS
            </h2>
          </Col>
          <Col sm={3} className="text-center game-header_icon d-flex justify-content-center">
            <Image src={coin} style={{ width: "50px", height: "50px" }} />
            <h1 className="ms-4 mt-2"> {exampleScore} Pts</h1>
          </Col>
          <Col sm={1} className="rules-button">
            <Image src={info} style={{ width: "50px", height: "50px" }} onClick={handleShow} />
          </Col>
        </Row>
        {/* game-section */}
        <Row className="justify-content-center text-center game-section_custom">
          {/* player section */}
          <Col sm={4} className="d-flex flex-column align-items-center game-section_player">
            <h1 style={{ textTransform: "capitalize" }}>{isLoading ? player.username : "player"}</h1>
            <Image src={rock} alt="batu" id="batu" style={{ width: "90px", height: "90px" }} onClick={() => handleClick("rock", cpuPick())} className={playerHands === "rock" ? "bg-selected" : ""} />
            <Image src={paper} alt="kertas" id="kertas" style={{ width: "90px", height: "90px" }} onClick={() => handleClick("paper", cpuPick())} className={playerHands === "paper" ? "bg-selected" : ""} />
            <Image src={scissors} alt="gunting" id="gunting" style={{ width: "90px", height: "90px" }} onClick={() => handleClick("scissor", cpuPick())} className={playerHands === "scissor" ? "bg-selected" : ""} />
          </Col>
          {/* message -- result */}
          <Col sm={2} className="d-flex flex-column align-items-center justify-content-between">
            <h1 style={{ fontSize: "4rem" }}>
              {playerScore}:{cpuScore}
            </h1>
            <h1 className="bg-default bg-selected" style={{ color: "#d40e00", fontSize: "2rem" }}>
              {result ? result : "VS"}
            </h1>
            <Image src={refresh} alt="refresh" id="refresh" style={{ width: "90px", height: "90px" }} onClick={() => refreshClick()} className="reset-button" />
          </Col>
          {/* cpu section */}
          <Col sm={4} className="d-flex flex-column align-items-center">
            <h1>Computer</h1>
            <Image src={rock} alt="batu-cpu" id="batu-cpu" style={{ width: "90px", height: "90px" }} className={cpuHands === "rock" ? "bg-selected" : ""} />
            <Image src={paper} alt="kertas-cpu" id="kertas-cpu" style={{ width: "90px", height: "90px" }} className={cpuHands === "paper" ? "bg-selected" : ""} />
            <Image src={scissors} alt="gunting-cpu" id="gunting-cpu" style={{ width: "90px", height: "90px" }} className={cpuHands === "scissor" ? "bg-selected" : ""} />
          </Col>
        </Row>
        {/* save-game-section */}
        <Row>
          {/* save button */}
          <Col className="d-flex flex-column align-items-center mb-3">
            <Button as={Link} to="/games/rps/score" variant="warning" type="submit" className="ps-5 pe-5" onClick={() => props.handleScore(exampleScore)}>
              <strong>Save</strong>
            </Button>
          </Col>
        </Row>
      </Container>
      {/* modal section - game rules */}
      <Modal show={show} onHide={handleClose} backdrop={true} centered animation={false}>
        <Modal.Header>
          <Modal.Title>Rules</Modal.Title>
          <Button onClick={handleClose} variant="secondary">
            <FontAwesomeIcon icon={faTimes} />
          </Button>
        </Modal.Header>
        <Modal.Body>
          <Container className="d-flex justify-content-center pb-4">
            <Image src={rules} style={{ width: "250px", height: "324px" }} />
          </Container>
        </Modal.Body>
      </Modal>
    </div>
  );
};

export default Game;
