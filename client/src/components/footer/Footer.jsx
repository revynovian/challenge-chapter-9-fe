import "./Footer.css";
import React from "react";
import { Container, Row, Col, Image } from "react-bootstrap";
import facebook from "../../assets/facebook.svg";
import twitter from "../../assets/twitter.svg";
import twitch from "../../assets/twitch.svg";
import vector from "../../assets/Vector.svg";
import { Link } from "react-router-dom";

const Footer = () => {
  return (

    <footer>
      <div className="footer--custom py-3">
        <Container>
          <Row className="pb-3 g-0">
            <Col md={8}>
              <ul className="d-flex justify-content-evenly">
                <li><Link to="/">main</Link></li>
                <li><a href="#gameplay">about</a></li>
                <li><a href="#features">game features</a></li>
                <li><a href="#requirement">system requirements</a></li>
                <li><a href="#topscore">quotes</a></li>
              </ul>
            </Col>
            <Col >
              <ul className="d-flex justify-content-evenly icon--custom">
                <li><Image src={facebook} className="me-4" fluid/></li>
                <li><Image src={twitter} className="me-4" /></li>
                <li><Image src={twitch} className="me-4" /></li>
                <li><Image src={vector} className="me-4" /></li>
              </ul>
            </Col>
          </Row>
          <Row className="pt-2 footer--custom_line">
            <Col md={4}>
              <ul className="d-flex justify-content-center justify-content-md-start">
                <li><p id="copyright">&#169; 2021 Your Games, Inc. All Right Reserved</p></li>
              </ul>
            </Col>
            <Col md={8}>
              <ul className="d-flex me-auto justify-content-center justify-content-md-end">
                <li><a href="#gameplay">privacy policy</a></li>
                <li><a href="#features">terms of service</a></li>
                <li><a href="#requirement">code of conducts</a></li> 
              </ul>
            </Col>
          </Row>
          </Container>
      </div>
    </footer>
  );
};

export default Footer;
