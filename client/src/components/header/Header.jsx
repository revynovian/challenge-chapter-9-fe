import './header.css'
import React, { useContext, useState } from "react";
import { Navbar, Nav, Container, Modal, Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSignOutAlt, faAtom } from '@fortawesome/free-solid-svg-icons';

// Import context
import { myAuthContext } from "../../context/authContext";

const Header = ({ isAuthenticated }) => {
  const authenticated = isAuthenticated;
  const { logout } = useContext(myAuthContext);

  // bootstrap modal
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const handlerLogout = async (e) => {
    e.preventDefault()
    setShow(false);
    await logout();
  };

  return (
    <div>
      <Navbar collapseOnSelect bg="dark" variant="dark" expand="lg" className="custom-navbar">
        <Container>
          <Navbar.Brand href="/">
          <FontAwesomeIcon icon={faAtom}/> BINAR GAMEHUB
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="me-auto">
              <Nav.Link as={Link} to="/">
                Home
              </Nav.Link>
              <Nav.Link  as={Link} to="/dashboard" >
                Dashboard
              </Nav.Link>
              <Nav.Link as={Link} to="/about">
                About Us
              </Nav.Link>
            </Nav>
            <Nav>
              {!authenticated && (
                <Nav.Link as={Link} to="/login">
                  Login
                </Nav.Link>
              )}
              {!authenticated && (
                <Nav.Link as={Link} to="/register">
                  Register
                </Nav.Link>
              )}
              {authenticated && (
                <Nav.Link  onClick={handleShow}>
                  <FontAwesomeIcon icon={faSignOutAlt}/>  Logout
                </Nav.Link>
              )}
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>

      {/* modal section */}
      <Modal show={show} onHide={handleClose} backdrop={true} centered animation={false}>
            <Modal.Body className="py-5 rounded">
              <h5>Are you sure you want to logout?</h5>
            </Modal.Body>
            <Modal.Footer>
              <Button variant="secondary" onClick={handleClose}>
                Cancel
              </Button>
              <Button variant="danger" as={Link} onClick={handlerLogout}  to="/login">Logout</Button>
            </Modal.Footer>
        </Modal>
    </div>
  );
};

export default Header;
