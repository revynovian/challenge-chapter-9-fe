import React, { useContext, useState } from "react";
import Header from "./header/Header";
import Home from "./home/Home";
import Footer from "./footer/Footer";
import Login from "./login/Login";
import Register from "./register/Register";
import Game from "./game/Game";
import Save from "./game/save/Save";
import Dashboard from "./dashboard/dashboard";
import GamePageSnake from "./game-details/snake-page/snake-page";
import GamePageRPS from "./game-details/rps-page/rps-page"; 
import GamePageXO from "./game-details/tictactoe-page/tictactoe-page"; 
import Profile from "./update-profile/update-profile";
import About from "./home/about-us/about-us"
import NotFoundPage from "./home/404-page/notfound"
import { BrowserRouter as Router, Switch, Route, Redirect } from "react-router-dom";

// Import context
import { myAuthContext } from "../context/authContext";

function App() {
  const { isAuthenticated } = useContext(myAuthContext);
  const [exampleScore, setScore] = useState(null);

  return (
    <div className="App">
      <Router>
        <Header isAuthenticated={isAuthenticated} />
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/about" exact component={About} />
          <Route path="/login" component={Login} />
          <Route path="/register" component={Register} />
          <Route path="/dashboard">{isAuthenticated ? <Dashboard /> : <Redirect to="/login" />}</Route>
          <Route path="/profile">{isAuthenticated ? <Profile /> : <Redirect to="/login" />}</Route>
          {/* rps gamepage details */}
          <Route path="/games/rps/score">{isAuthenticated ? <Save exampleScore={exampleScore} /> : <Redirect to="/login" />}</Route>
          <Route path="/games/rps/play">{isAuthenticated ? <Game handleScore={(score) => setScore(score)} /> : <Redirect to="/login" />}</Route>
          <Route path="/games/rps">{isAuthenticated ? <GamePageRPS /> : <Redirect to="/login" />}</Route>
          {/* example games details */}
          <Route path="/games/snake">{isAuthenticated ? <GamePageSnake /> : <Redirect to="/login" />}</Route>
          <Route path="/games/tictactoe">{isAuthenticated ? <GamePageXO /> : <Redirect to="/login" />}</Route>
          <Route path="*"><NotFoundPage/></Route>
        </Switch>
        <Footer />
      </Router>
    </div>
  );
}

export default App;
