
import React , { useEffect, useState } from "react";
import { Container, Row, Col } from "react-bootstrap";
import GameBoard from "../game-board";
import LeaderBoard from "../leaderboard-table";
import Axios from "axios";

import GameList from "../../dashboard/gamedata/gamelist-library";


const SnakePage = () => {
  const accessToken = localStorage.getItem("token");
  const url = "https://backend-app-chapter-9.herokuapp.com/user/";

  const [Players, setPlayers] = useState([])
  const [isLoading, setisLoading] = useState(false)
  useEffect (() => {
    Axios.get(url, {
      headers: {Authorization : accessToken}
    })
    .then ( res => {
      setisLoading(true)
      const data = (res.data.message)
      setPlayers(data)
    })
    .catch ( err => {
      console.log(err)
    });
  },[url, accessToken]) 

    // sorting data to get rank
  // filter spesific data for leaderboard table
  const getData = [];
  Players.forEach((e) => {
    const username = e.username;
    const city = e.User_Detail.city;
    const score = e.User_Score.tic_tac_toe;
    const tobeSort = {
      username,
      city,
      score
    };
    getData.push(tobeSort);
  });
  // sorting array
  const rankedPlayers = getData.sort((a, b)=> (a.score < b.score) ? 1 : -1)
  
  return (
    <>
    <div className="dashboard-custom"> 
      <Row>
        <Col>
        <Container>
          {/* game description form static data*/}
          <GameBoard 
            gameName={GameList[2].title} 
            gameDescription={GameList[2].description}  
            gameLink={GameList[2].link}
            gameCover={GameList[2].coverImage} />
          <LeaderBoard isLoad={isLoading} dataPlayers={rankedPlayers}/>
          </Container>
        </Col>
      </Row>
    </div>
    </>
  );
};

export default SnakePage;
