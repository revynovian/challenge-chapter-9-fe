import React from "react";
import { Row, Col, Table, Spinner } from "react-bootstrap";

const LeaderBoard = ({isLoad,dataPlayers}) => {

  return (
  <>
    {/* leaderboard table section*/}
    <Row className="mt-3" >
      <Col>
      <h2 className="text-white">Leaderboard</h2>
      <Table hover bordered className="mb-5 mt-3" variant="dark">
        <thead >
          <tr>
            <th style={{width : "8%"}}>Rank</th>
            <th>Username</th>
            <th>City</th>
            <th>Gamepoints</th>
          </tr>
        </thead>
        <tbody>
          {(isLoad)? dataPlayers.map((item, index) => {
            return (
              <tr key={index}>
                <td>{index+1}</td>
                <td>{item.username}</td>
                <td>{item.city}</td>
                <td>{item.score}</td>
              </tr>
            )}) : 
              <tr>
              <td className="text-center" colSpan="4">
                <Spinner animation="border" role="status" variant="warning" />
              </td>
              </tr>}
        </tbody>
      </Table>
      </Col>
    </Row>
  </>
  )
}

export default LeaderBoard;