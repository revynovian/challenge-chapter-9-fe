
import React , { useEffect, useState } from "react";
import { Container, Row, Col } from "react-bootstrap";
import GameBoard from "../game-board";
import LeaderBoard from "../leaderboard-table";
import Axios from "axios";

import GameList from "../../dashboard/gamedata/gamelist-library";

const RpsPage = () => {
  const accessToken = localStorage.getItem("token");
  const url = "https://backend-app-chapter-9.herokuapp.com/user/";

  const [Players, setPlayers] = useState([])
  const [isLoading, setisLoading] = useState(false)
  // fetch data all player
  useEffect (() => {
    Axios.get(url, {
      headers: {Authorization : accessToken}
    })
    .then ( res => {
      setisLoading(true)
      const data = (res.data.message)
      setPlayers(data)
    })
    .catch ( err => {
      console.log(err)
    });
  },[url, accessToken]) 
  
  // sorting data to get rank
  // filter spesific data for leaderboard table
  const getData = [];
  Players.forEach((e) => {
    const username = e.username;
    const city = e.User_Detail.city;
    const score = e.User_Score.rock_paper_scissor;
    const tobeSort = {
      username,
      city,
      score
    };
    getData.push(tobeSort);
  });
  // sorting array
  const rankedPlayers = getData.sort((a, b)=> (a.score < b.score) ? 1 : -1)

  return (
    <>
    <div className="dashboard-custom"> 
      <Row>
        <Col>
        <Container>
          <GameBoard 
            gameName={GameList[0].title} 
            gameDescription={GameList[0].description}  
            gameLink={GameList[0].link}
            gameCover={GameList[0].coverImage}/>
          <LeaderBoard isLoad={isLoading} dataPlayers={rankedPlayers}/>
          </Container>
        </Col>
      </Row>
    </div>
    </>
  );
};

export default RpsPage;
