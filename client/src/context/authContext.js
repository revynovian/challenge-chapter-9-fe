import React, { createContext, useState } from "react";

export const myAuthContext = createContext({});

function useAuthProvider() {
  const token = localStorage.getItem("token");
  const [isAuthenticated, setIsAuthenticated] = useState(() => (token ? true : false));

  const login = async (accessToken, userID) => {
    localStorage.setItem("token", accessToken);
    localStorage.setItem("userID", userID);
    setIsAuthenticated(true);
  };

  const logout = async () => {
    localStorage.removeItem("token");
    localStorage.removeItem("userID");
    setIsAuthenticated(false);
  };

  return {
    isAuthenticated,
    setIsAuthenticated,
    login,
    logout,
  };
}

const AppContext = (props) => {
  const auth = useAuthProvider();

  return <myAuthContext.Provider value={auth}>{props.children}</myAuthContext.Provider>;
};

export default AppContext;
